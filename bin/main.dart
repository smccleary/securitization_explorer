import 'package:ansicolor/ansicolor.dart';
import 'package:args/args.dart';
import 'package:securitization_explorer/securitization_explorer.dart';

ArgResults args;

main(List<String> arguments) {
  final ArgParser argParser = new ArgParser();
  argParser.addOption('libor', abbr: 'l', defaultsTo: '0.05');
  argParser.addOption('duration', abbr: 'd', defaultsTo: '4.5');

  args = argParser.parse(arguments);

  // ansi pens
  AnsiPen e = new AnsiPen()
    ..white()
    ..red();
  AnsiPen d = new AnsiPen()
    ..white()
    ..rgb(r: 1.0, g: 1.2, b: 0.8);
  AnsiPen i = new AnsiPen()
    ..white()
    ..blue();
  AnsiPen t = new AnsiPen()..white(bold: true)..white();
  AnsiPen ok = new AnsiPen()
    ..white(bold: true)
    ..rgb(r: 0.0, g: 1.0, b: 0.0);
  AnsiPen input = new AnsiPen()
    ..yellow(bg: true)
    ..rgb(r: 0.0, g: 0.0, b: 0.0);

  var sec = new Securitization(
    symbol: 'FORSAB',
    libor: double.parse(args['libor']),
    annualPayoutTarget: 6000000,
    name: 'Forsynth Metals AAA Bond',
    collateral: 90000000.00,
    collateralDuration: double.parse(args['duration']),
  );

  sec.addLayer(new Tranch(par: 30000000.0, rate: 0.0, name: '(L * par)'), 0.0);
  sec.addLayer(new Tranch(par: 30000000.0, rate: 0.12, name: '(R - L) * P'), 202500000);
  sec.addLayer(new Tranch(par: 30000000.0, rate: 0.13, name: '(R - L) * P'), 202500000);

  // todo: for eacn tranch we need to create a cash flow
  print(sec.toString());
  print('---------------------------------------------------------------');
  print('> LIBOR (%):\t\t\t${input(sec.libor.toString())}');
  print(t('> Tranches / Loss Layers'));
  print('\tCashflow:\t\t${i(sec.annualPayoutTarget.toString())}');
  sec.cashFlows.forEach((c) => print(d('\t' + c.toString())));
  var csPres =
      sec.getCashFlowPreserved() < sec.annualPayoutTarget ? e(sec.getCashFlowPreserved().toString()) : ok(sec.getCashFlowPreserved().toString());
  print('\tCashflow Preserved:\t${csPres}');

  print('\n> Collateral Duration:\t\t${input(sec.collateralDuration.toString())}');
  print('> Collateral Duration Amount:\t${i(sec.collateralDurationAmount.toString())}');
  print(t('> Risk Distibution'));
  sec.riskClaims.forEach((c) => print(d('\t' + c.toString())));
  var rPres = sec.getRiskPreserved() < sec.collateralDurationAmount ? e(sec.getRiskPreserved().toString()) : ok(sec.getRiskPreserved().toString());

  print('\tRisk Preserved:\t\t${rPres}');
}
