import 'package:ansicolor/ansicolor.dart';

int calculate() {
  return 6 * 7;
}

class UTILS {
  static const double Libor = 0.05;
}

class Tranch {
  int id;
  double par;
  double rate;
  String name;

  Tranch({this.par, this.rate, this.name});
}

class CashFlow {
  double payout;
  double par;
  double rate;
  int tranch;
  String when;
  String memo;

  int id;

  CashFlow({this.payout, this.tranch, this.rate, this.memo, this.par}) {
    this.when = 'yr.';
  }

  @override
  String toString() {
    return '[TRANCH:${id.toString()}]\trate: ${(rate * 100).toString()}%\tpayout: ${payout.toString()} $when ${memo}';
  }
}

class RiskClaim {
  double amount;
  double liability;
  int tranch;
  double _duration;

  double get duration => this.amount / this.liability;

  RiskClaim({this.amount, this.tranch, this.liability});

  @override
  String toString() {
    return '[TRANCH:${tranch.toString()}]\tamount: ${amount.toString()}\tduration: ${duration.toString()}';
  }
}

class Securitization {
  String name;
  double collateral;
  double collateralDuration;
  double annualPayoutTarget;
  double _collateralDurationAmount;
  double libor;
  String symbol;
  List<Tranch> tranches = [];
  List<CashFlow> _cashFlows = [];
  List<RiskClaim> _riskTransfer = [];

  @override
  String toString() {
    return '$name\t$collateral';
  }

  Securitization({
    this.annualPayoutTarget,
    this.symbol,
    this.name,
    this.collateral,
    this.collateralDuration,
    this.libor,
    this.tranches,
  }) {
    this.tranches = [];
    this._cashFlows = [];
    this._riskTransfer = [];
  }

  addRiskHandler(RiskClaim claim) {
    this._riskTransfer.add(claim);
  }

  //

  addLayer(Tranch tranch, double claimAmount) {
    tranch.id = this.tranches.length + 1;

    var cashFlow = new CashFlow(
      payout: 0.0,
      tranch: tranch.id,
      rate: tranch.rate,
      memo: tranch.name,
    );

    if (tranch.rate == 0.0) {
      cashFlow.payout = this.libor * tranch.par;
    } else {
      cashFlow.payout = (tranch.rate - this.libor) * tranch.par;
    }

    cashFlow.id = this._cashFlows.length + 1;
    cashFlow.par = tranch.par;
    this._cashFlows.add(cashFlow);

    // add a risk claim ticket
    this.addRiskHandler(new RiskClaim(amount: claimAmount, tranch: tranch.id, liability: tranch.par));

    this.tranches.add(tranch);
  }

  calculateCashFlows() {}

  setRiskDuration(RiskClaim risk) {}

  setCollateralDuration() {}

  double getCashFlowPreserved() => this._cashFlows.fold(0, (t, e) => t + e.payout);

  double getRiskPreserved() {
    return this._riskTransfer.fold(0, (t, e) => t + e.amount);
  }

  double get collateralDurationAmount => this.collateralDuration * this.collateral;

  List<CashFlow> get cashFlows => _cashFlows;

  List<RiskClaim> get riskClaims => _riskTransfer;
}
